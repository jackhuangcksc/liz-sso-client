import store from '../store'

export function deleteUserInfo() {
    deleteCookie('userinfo')
}

export function setUserInfo(userInfo) {
    setCookie('userinfo', JSON.stringify(userInfo))
}

export function getUserInfo() {
    let userInfo = getCookie("userinfo");
    if (!!userInfo) {
        userInfo = JSON.parse(userInfo)
        if (!userInfo.userName) {
            userInfo.userName = store.getters.userInfo.userName
        }
        if (!userInfo.accessToken) {
            userInfo.accessToken = store.getters.userInfo.accessToken
        }
        if (!userInfo.avatar) {
            userInfo.avatar = store.getters.userInfo.avatar
        }
    } else {
        userInfo = {
            userName: store.getters.userInfo.userName,
            accessToken: store.getters.userInfo.accessToken,
            avatar: store.getters.userInfo.avatar
        }
    }
    setCookie('userinfo', JSON.stringify(userInfo))
    return userInfo
}

export function setCookie(name, value) {
    var exp = new Date();
    exp.setTime(exp.getTime() + 30 * 60 * 1000);
    document.cookie = "sso.admin." + name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

export function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + "sso.admin." + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}

export function deleteCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = "sso.admin." + name + "=" + cval + ";expires=" + exp.toGMTString();
}