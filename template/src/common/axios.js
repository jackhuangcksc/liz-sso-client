import axios from "axios";
import qs from "qs";

import { getBaseUrl } from "../common/utils";
import { MessageBox } from "element-ui";
import store from "../store/index";
import * as cache from "./cookie"

// axios 配置
axios.defaults.timeout = 10000;
//axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
//axios.defaults.baseURL = 'http://localhost:8008';
/* axios.defaults.baseURL = getBaseUrl(window.location.href);
 */

//POST传参序列化
axios.interceptors.request.use((config) => {
  if (config.method === 'post') {
    config.data = qs.stringify(config.data);
  }
  let userInfo = cache.getUserInfo()
  config.headers['Liz-Access-Token'] = userInfo.accessToken
  return config;
}, (error) => {
  return Promise.reject(error);
});

//返回状态判断
axios.interceptors.response.use(function (response) {

  return response
}, function (error) {
  // Do something with response error  
  return Promise.reject(error)
})


export function fetch(url, config = { method: 'get' }) {
  return axios.request({ ...config, url })
  // return new Promise((resolve, reject) => {
  //   axios.request({ ...config, url })
  //     .then(response => {
  //       resolve(response.data);
  //     }, err => {
  //       reject(err);
  //     })
  //     .catch((error) => {
  //       reject(error)
  //     })
  // })
}

export default axios
