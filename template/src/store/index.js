import Vue from "vue";
import Vuex from "vuex";
import types from "./mutation-types";
import * as cookie from "../common/cookie";
import * as api from "../services/api";
import { getNavList } from '../services/base'
import { getCurrentMenu, getSessionKey } from '../common/utils'
import router from '../router'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,  // process.env.NODE_ENV !== 'production', 直接修改state 抛出异常

  getters: {
    loading: state => state.loading,
    menuList: state => state.menuList,
    sidebar: state => state.sidebar,
    userInfo: state => state.userInfo,
    device: state => state.device,
    currentMenu: state => state.currentMenu,
    currentRole: state => state.currentRole,
    pageLoading: state => state.pageLoading,
    pageLoadingTitle: state => state.pageLoadingTite
  },
  state: {
    loading: false,
    menuList: {},
    sidebar: {
      collapsed: cookie.getCookie('sidebar.collapsed') === 'true',
      show: cookie.getCookie('sidebar.show') === 'true',
    },
    device: {
      isMobile: false
    },
    userInfo: {
      userType: -1,
      userName: '',
      accessToken: '',
      avatar: './static/img/avatar.png'
    },
    currentMenu: {},
    currentRole: {
      query: false,
      insert: false,
      update: false,
      delete: false
    },
    pageLoading: false,
    pageLoadingTitle: ""
  },
  mutations: {
    //只能同步的函数
    [types.TOGGLE_DEVICE](state, isMobile) {
      state.device.isMobile = isMobile
    },
    [types.TOGGLE_LOADING](state) {
      state.loading = !state.loading
    },
    [types.LOAD_MENU](state, menu) {
      state.menuList = menu;
    },
    [types.LOAD_CURRENT_MENU](state, menu) {
      state.currentMenu = menu;
    },
    [types.SET_CURRENT_ROLE](state, role) {
      state.currentRole = role;
    },
    [types.SET_USER_INFO](state, info) {
      state.userInfo = info;
    },
    [types.TOGGLE_SIDEBAR](state, collapsed) {
      if (collapsed == null) collapsed = !state.sidebar.collapsed;
      state.sidebar.collapsed = collapsed;
      cookie.setCookie("sidebar.collapsed", collapsed)
    },
    [types.TOGGLE_SIDEBAR_SHOW](state, show) {
      if (show == null) state.sidebar.show = !state.sidebar.show;
      else {
        state.sidebar.show = show;
      }
      cookie.setCookie("sidebar.show", state.sidebar.show)
    },
    [types.SET_PAGE_LOADING](state, pageLoading, pageLoadingTitle) {
      if (!pageLoadingTitle) {
        pageLoadingTitle = ""
      }
      state.pageLoading = pageLoading;
      state.pageLoadingTitle = pageLoadingTitle;


    }
  }, actions: {
    //异步的函数
    toggleLoading: ({ commit }) => commit(types.TOGGLE_LOADING),

    loadMenuList: ({ commit }, vm) => {
      getNavList(vm).then((r) => {
        if (r.code == 0) {
          commit(types.LOAD_MENU, r.result)
        }
      })

    },
    setUserInfo: ({ commit }, userInfo) => {
      commit(types.SET_USER_INFO, userInfo);
    }
  },
})

export default store
