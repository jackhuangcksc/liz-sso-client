import axios from "../common/axios";
import { doRequest } from './index'
import { BASE } from "./api";
import ROUTE_MODEL from '../router/route-model'
import * as cache from "../common/cookie";
/**
 * 获取菜单
 */
export function getNavList(vm) {
    return doRequest(BASE.NAVBAR_LIST, vm, ROUTE_MODEL.QUERY)
}

export function doLogin(vm, params) {
    let data = {
        username: params.username,
        password: params.password
    };

    return new Promise((res, rej) => {
        axios
            .request({
                method: "post",
                url: BASE.LOGIN,
                data: data,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(r => {
                res(r.data)
            }).catch(e => {
                rej()
            })
    })


}

/**
 * 退出登录
 */
export function doLogout(vm) {
    let userInfo = cache.getUserInfo()
    doRequest(BASE.LOGOUT, vm, {}, false).then((result) => {
        if (result.code == 0) {
            cache.setUserInfo({})
            window.location.replace(BASE.INDEX);
        }
    })
}

/**
 * 获取当前路由权限
 * @param {} vm 
 */
export function getRoleRoute(vm) {
    return doRequest(BASE.ROLE_ROUTE, vm, ROUTE_MODEL.QUERY)
}

