import config from "../config";

/*
*按模块区分
*/


export const BASE = {
    INDEX: config.staticApi + '/',
    LOGIN: config.webApi + '/login',
    LOGOUT: config.webApi + '/logout',
    TOKEN_VALIDATE: config.ssoApi + '/token/validate',
    NO_PERM: config.ssoApi + '/permission',
    ROLE_ROUTE: config.ssoApi + '/role/route',
    NAVBAR_LIST: config.webApi + '/role/nav/list'
}