import axios from "../common/axios";
import * as cache from "../common/cookie";
import * as api from "./api";
import * as utils from "../common/utils";
import types from "../store/mutation-types"
import store from "../store";

import config from "../config";

export function validateAuth(vm, params) {
    let userInfo = cookie.getUserInfo()

    const accessToken = utils.getQueryString('access_token')
    const userName = utils.getQueryString('user_name')
    let isReturn = false
    if (null != accessToken && "" != accessToken) {
        userInfo.accessToken = accessToken
        isReturn = true
    }

    if (null != userName && "" != userName) {
        userInfo.userName = userName
        isReturn = true
    }

    if (isReturn) {
        window.location.replace(api.INDEX);
        cookie.setUserInfo(userInfo)
    } else {

    }
}

function doAuth() {
    const returnUrl = encodeURIComponent(api.INDEX)
    window.location.replace(api.LOGIN + '?app_id=' + config.appId + '&return_url=' + returnUrl);
}

function noPerm(vm) {
    const returnUrl = encodeURIComponent(api.INDEX)
    let userInfo = cookie.getUserInfo()
    window.location.replace(api.NO_PERM + '?return_url=' + returnUrl + '&access_token=' + userInfo.accessToken);
}


export function doRequest(api, vm, params = {}, isGet = true, routeModel = 'query') {
    return new Promise((resRequest, rejRequest) => {
        let config = {
            method: isGet ? 'get' : 'post',
            url: api,
            data: params,
            headers: {
                'Content-Type': isGet ? 'application/json' : 'application/x-www-form-urlencoded',
                'Liz-Route': vm ? vm.$route.path : '',
                'Liz-Route-Model': routeModel
            }
        }

        axios.request(config).then(response => {
            if (null != response) {
                if (response.data.code == 40005) {
                    doAuth()
                } else if (response.data.code == 40006) {
                    /*     vm.$notify.error({
                            title: '错误',
                            message: '您没有进行该操作的权限'
                        });
                        resRequest(response.data) */
                    noPerm(vm)
                } else {
                    resRequest(response.data)
                }

            } else {
                doAuth()
                /*               rejRequest() */
            }
        }).catch((error) => {
            /*         doAuth() */
            /*             rejRequest(error) */
        })
    })
}