import Vue from "vue";
import VueRouter from "vue-router";
import routeConfig from "./routes";
import { sync } from "vuex-router-sync";
import store from "../store";
import types from "../store/mutation-types";
import * as cache from "../common/cookie";
import { validateAuth } from "../services"
//加载路由中间件
Vue.use(VueRouter)

//定义路由
const router = new VueRouter({
  routes: routeConfig,
  //mode: 'history'
})

sync(store, router)

const { state } = store

router.beforeEach((to, from, next) => {
  if (state.device.isMobile && state.sidebar.opened) {
    store.commit(types.TOGGLE_SIDEBAR, false)
  }
  validateAuth(this)
  next()
})


export default router
