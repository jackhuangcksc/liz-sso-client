/* 
import NotFound from "../components/NotFound.vue";
import App from "../App.vue";

import Login from "../pages/Login.vue";
import Dashboard from "../pages/Dashboard.vue";

import SystemUserList from "../pages/system/user/UserList.vue";
import SystemUserAdd from "../pages/system/user/UserAdd.vue";
import SystemUserEdit from "../pages/system/user/UserEdit.vue"; */

const NotFound = () => import(/* webpackChunkName: "group-pub" */ '../components/NotFound.vue')
const App = () => import(/* webpackChunkName: "group-pub" */ '../App.vue')
const Dashboard = () => import(/* webpackChunkName: "group-pub" */ '../pages/Dashboard.vue')

// Routes
const routes = [
  {
    path: '',
    component: App,
    children: [
      { path: '/dashboard', name: 'Dashboard', component: Dashboard },
      { path: '/', name: 'Main', component: Dashboard }
    ]
  },
  { path: '*', name: 'NotFound', component: NotFound }
]


export default routes

